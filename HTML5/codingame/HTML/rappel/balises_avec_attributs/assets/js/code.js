// Add yours functions JavaScript here :)

// Fonction feuille chargée
function sheetLoaded() {
/**
 * Envoyer un message vers la console de développement, sachant que la feuille
 * de style CSS a été chargée
 */
console.log("Succès du chargement de notre feuille de style !");
}
// Fonction feuille erreur au chargement de votre page
function sheetError() {
/**
 * Envoyer un message vers la console de développement, sachant que la feuille
 * de style CSS n'a pas été chargée
 */
console.log("Erreur lors du chargement de notre feuille de style !");
}
